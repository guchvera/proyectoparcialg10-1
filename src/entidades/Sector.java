/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**Declaracion de la clase Sector.
 *
 * @author Jose
 * @version 09/07/2019
 */
public enum Sector {
    NORTE, SUR, ESTE, OESTE, CENTRO    
}
